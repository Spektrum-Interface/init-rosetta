## Init system rosetta

This is where I (Night_H4nter) do my work on init rosetta, as well as process
others' contibutions to it.

Init rosetta was initially started by 'aldum' and 'unrooted' (and maybe someone else)
[here](https://gitea.artixlinux.org/aldum/init_rosetta/src/branch/master/init_rosetta.md),
and it's main goal is to ease getting started with using "alternative" init
systems for all users in general, with additional notes for Artix users in
particular. It covers the init systems supported in Artix, and systemd, which
most people presumably migrate from.

See [init_rosetta.md](./init_rosetta.md).
